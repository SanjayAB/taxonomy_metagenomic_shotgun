# Download aligned fasta database from RDP for bacteria and archaea

wget http://rdp.cme.msu.edu/download/current_Bacteria_aligned.fa.gz

wget http://rdp.cme.msu.edu/download/current_Archaea_aligned.fa.gz


# Unzip the databases
gunzip *.gz
