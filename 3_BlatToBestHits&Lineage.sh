#!/bin/sh
#SBATCH --job-name=222-short
#SBATCH --nodes=1
#SBATCH --qos=npod
#SBATCH --ntasks-per-node=8
#SBATCH --time=06:00:00
#SBATCH --mem=50gb
#SBATCH --output=222-short.txt
#SBATCH --error=222-short.txt
#SBATCH --mail-user=sanjay.antonybabu@gmail.com
#SBATCH --mail-type=ALL
Diets="222"
for d in ${Diets}
	do
echo -n  ''  > ${d}-16S_rRNA_BestHits.txt
IFS=$'\n'
		for L in $(cat ${d}-BLAT-output | cut -f1 | sort | uniq)
			do
				grep "${L}" ${d}-BLAT-output | sort -k12nr | head -1 | cut -f1,2 | tr "|" "	"  >> ${d}-16S_rRNA_BestHits.txt
			done
	done
	
	
for d in ${Diets}
	do
	
		cat ${d}-16S_rRNA_BestHits.txt | cut -f3 | sort | uniq -c > ${d}-16S_rRNA_LineageCounts.txt
	
	done