#!/bin/sh
#SBATCH --job-name=16S_LQ
#SBATCH --nodes=1
#SBATCH --qos=npod
#SBATCH --ntasks-per-node=8
#SBATCH --time=160:00:00
#SBATCH --mem=50gb
#SBATCH --output=16S_LQ.txt
#SBATCH --error=16S_LQ.txt
#SBATCH --mail-user=sanjay.antonybabu@gmail.com
#SBATCH --mail-type=ALL


module load blat/35x1

blat ForBLAT-current_Prokaryotes_unaligned.fa COM-final-nuc-file COM-16S-BLAT -q=dna -t=dna -out=blast8
blat ForBLAT-current_Prokaryotes_unaligned.fa HQ-final-nuc-file HQ-16S-BLAT -q=dna -t=dna -out=blast8
blat ForBLAT-current_Prokaryotes_unaligned.fa LQ-final-nuc-file LQ-16S-BLAT  -q=dna -t=dna -out=blast8


