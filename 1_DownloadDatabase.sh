#!/bin/sh
#SBATCH --job-name=Download
#SBATCH --nodes=1
#SBATCH --qos=short
#SBATCH --ntasks-per-node=1
#SBATCH --time=1:00:00
#SBATCH --mem=50gb
#SBATCH --output=Download.txt
#SBATCH --error=Download.txt
#SBATCH --mail-user=sanjay.antonybabu@gmail.com
#SBATCH --mail-type=ALL


wget http://rdp.cme.msu.edu/download/current_Bacteria_unaligned.fa.gz
wget http://rdp.cme.msu.edu/download/current_Archaea_unaligned.fa.gz

gunzip *.gz